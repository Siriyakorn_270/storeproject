/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author gaiga
 */
public class Receipt {
    private int id;
    private Date created;
    private User seller;
    private Customer customer;
    private ArrayList<ReceiptDetail> receipDetail;

    public Receipt(int id, Date created, User seller, Customer customer) {
        this.id = id;
        this.created = created;
        this.seller = seller;
        this.customer = customer;
        receipDetail = new ArrayList<>();   
    }

    public Receipt(User seller, Customer customer) {
        this(-1,null,seller,customer);
    }
    public void addReceiptDetail(int id, Product product, int amount ,double price){
        for(int row = 0; row<receipDetail.size(); row++){
            ReceiptDetail r = receipDetail.get(row);
            if(r.getProduct().getId() == product.getId()){
                r.addAmount(amount);
                return;
            }
        }
        receipDetail.add(new ReceiptDetail(id,product, amount,price, this));
    }
    public void addReceiptDetail(Product product, int amount){
        addReceiptDetail(-1,product, amount,product.getPrice());
    }
    public void deleteReceiptDetail(int row){
        receipDetail.remove(row);
    }
    public double getTotal(){
        double total = 0;
        for(ReceiptDetail r: receipDetail){
            total = total + r.getTotal();
        }
        return total;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<ReceiptDetail> getReceipDetail() {
        return receipDetail;
    }

    public void setReceipDetail(ArrayList<ReceiptDetail> receipDetail) {
        this.receipDetail = receipDetail;
    }

    @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id 
                + ", created=" + created 
                + ", seller=" + seller 
                + ", customer=" + customer 
                + ", total=" + this.getTotal()
                + "}\n";
        for(ReceiptDetail r: receipDetail ){
            str = str + r.toString() + "\n";
        }
        return str;
    }
  
}
